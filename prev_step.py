LIMIT = 9

def prev_digit(num_list: list[int], limit: int) -> int:
    if num_list[0] > limit + 1:
        num_list[0] -= 1
        return int(''.join([str(i) for i in num_list[::-1]]))
    elif len(num_list) != 3:
        return prev_digit(num_list[1:], num_list[2])
    else:
        return int(''.join([str(10 - i) for i in list(range(len(num_list) + 1, 0, -1))]))

def convert_to_list(num: int) -> list:
    return [int(i) for i in str(num)[::-1]]

def prev_reading(num: int) -> int:
    num_list = convert_to_list(num)
    size = len(num_list)
    prev_reading = [int(i) for i in str(prev_digit(num_list, num_list[1]))]
    while len(prev_reading) < size:
        if prev_reading[-1] != LIMIT:
            prev_reading.append(prev_reading[-1] + 1)
        else:
            prev_reading.insert(0, prev_reading[0] - 1)
    return int(''.join([str(i) for i in prev_reading]))
