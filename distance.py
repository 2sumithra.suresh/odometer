import next_step

def distance(reading1: int, reading2: int) -> int:
    if len(str(reading1)) != len(str(reading2)):
        return "Invalid"
    step = reading1
    count = 0
    while step != reading2:
        count += 1
        step = next_step.next_reading(step)
    return count
        
print(distance(1234,1245))
