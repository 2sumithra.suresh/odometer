import next_step

def reading(reading1: int, steps: int) -> int:
    reading = reading1
    for i in range(steps):
        reading = next_step.next_reading(reading)
    return reading

print(reading(1234, 6))
        
