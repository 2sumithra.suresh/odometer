LIMIT = 9

def next_digit(num_list: list[int], limit: int) -> int:
    if num_list[0] < limit:
        num_list[0] += 1
        return int(''.join([str(i) for i in num_list[::-1]]))
    elif len(num_list) != 1:
        return next_digit(num_list[1:], limit - 1)
    else:
        return int(''.join([str(i) for i in list(range(1, len(num_list) + 2))]))

def convert_to_list(num: int) -> list:
    return [int(i) for i in str(num)[::-1]]

def next_reading(num: int) -> int:
    num_list = convert_to_list(num)
    size = len(num_list)
    next_reading = [int(i) for i in str(next_digit(num_list, LIMIT))]
    while len(next_reading) < size:
        next_reading.append(next_reading[-1] + 1)
    return int(''.join([str(i) for i in next_reading]))
